vendor:
	mkdir -p vendor

.PHONY: libphonenumber
libphonenumber: vendor
	cd vendor && svn checkout http://libphonenumber.googlecode.com/svn/trunk/ libphonenumber
	cd vendor && svn checkout http://closure-library.googlecode.com/svn/trunk/ closure-library
	cd vendor && svn checkout http://closure-compiler.googlecode.com/svn/trunk/ closure-compiler
	cd vendor && svn checkout http://closure-linter.googlecode.com/svn/trunk/ closure-linter
	cd vendor && svn checkout http://python-gflags.googlecode.com/svn/trunk/ python-gflags

.PHONY: test-libphonenumber
test-libphonenumber:
	open vendor/libphonenumber/javascript/i18n/phonenumbers/phonenumberutil_test.html
	open vendor/libphonenumber/javascript/i18n/phonenumbers/asyoutypeformatter_test.html

js/metadata.js:
	cp vendor/libphonenumber/javascript/i18n/phonenumbers/metadata.js js/metadata.js.orig
	sed -f scripts/fix-metadata.sed js/metadata.js.orig > js/metadata.js
	rm js/metadata.js.orig

.PHONY: flag_icons
flag_icons:
	cd vendor && curl -O http://www.famfamfam.com/lab/icons/flags/famfamfam_flag_icons.zip
	cd vendor && unzip famfamfam_flag_icons.zip -d flags

.PHONY: sprite
sprite:
	scripts/make-flag-sprites.sh

clean:
	rm js/metadata.js
