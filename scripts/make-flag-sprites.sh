#!/bin/sh

echo "Creating sprite sheet"

XPOS=0
CSS_FILE=css/flags.css
SPRITE_FILE=img/flags.png

mkdir -p css
mkdir -p img

cat > $CSS_FILE <<EOF
.flag {
    display: inline-block;
    width: 16px;
    height: 11px;
    line-height: 14px;
    background-image: url('../${SPRITE_FILE}');
    background-repeat: no-repeat;
}
EOF
gm convert -page +$XPOS+0 vendor/flags/png/ad.png -mosaic $SPRITE_FILE

for IPATH in vendor/flags/png/*.png ; do
    CUR_FILE="${IPATH##*/}"
    CUR_NAME="${CUR_FILE%%.png}"

    gm convert -page +0+0 $SPRITE_FILE -page +$XPOS+0 $IPATH -mosaic $SPRITE_FILE
    cat >> $CSS_FILE <<EOF
.$CUR_NAME { background-position:-${XPOS}px 0 }
EOF
    let XPOS=XPOS+16
done

echo "Wrote ${CSS_FILE}"
echo "Wrote ${SPRITE_FILE}"
echo "Done"
